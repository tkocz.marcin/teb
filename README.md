# Teb

# gitlab

git clone https://gitlab.com/tkocz.marcin/teb.git
mkdir nazwafolderu   `stworzy folder`

cd nazwafolderu `wejdzie do folderu`


 git add . 

 git commit -m "wiadomosc"

 git push origin main


# Zaliczenie części HTML,CSS
- Zadania proszę przesłać do marca, 
- są dobrym sposobem na pokazanie czegoś w CV, więc będę robił `CR` (`Code Review` czyli przeglądał kod i podpowiadał poprawki), zeby był jak najlepszy :)
- Na ich podstawie będzie wystawiana ocena z bloku zajęć: HTML/CSS. 
- ważne aby strona wyglądała dobrze na różnych wielkościach ekranów


# Zaliczenie części JS
 - Demo : https://tkocz-marcin.github.io/jq/ - nie wszystkie funkcjonalności są dokończone
 - Zadania należy wysłać do marca 2022,
 - Kod będzie przechodził `CR` aż do zadowalającego poziomu,
 - Można używać JS, JQuery i innych frameworków i bibliotek,
 - Ocena z działu Język JavaScript (w tym biblioteki JS),
 - zadanie jest fajne do CV, można go zrobić w różnych technologiach pożądanych na rynku (`React`, `Angular`, `Vue` itp)

## Działanie:
### Otwiernie menu
- Menu jest wysywane z lewej strony, nalezy je polaczyc z guzikiem w prawym górnym rogu.
- Po kliknięciu menu powinno się wysuwać a guzik pokazywać X
- Po kolejnym kliknięciu menu powinno się chować a guzik pokazywać hamburgera (3 poziome linie)

### Formularz
Przed wysłaniem formularza musimy sprawdzić czy:

- hasła są takie same,
- Hasło musi mieć minimum 5 znaków,
- email musi zawierać @ np: test@op.pl
- Przycisk pokaż hasło musi pokazywac hasło zamiast *****
- Jezeli hasło jest złe dajemy komunikat Błędne hasło
- Jezeli email jest zły to dajemy info Błędny email
- Jezeli wszystko ok dajemy info Formularz ok!
- class dla komunikatów: ok: alert-success, błędy: alert-warning
- jezeli jest komunikat wyswietlony a zaczynamy cos pisac w fomularzu to chowamy komunikat
- po poprawnym zalogowaniu robimy formularz do obliczenia BMI. 
### Stopka niezalogowany:
- W stopce wyświetlamy komunikat `witaj przybyszu! ` + aktualna data w formacie 24.12.2021

### Stopka zalogowany:
- przycisk do wylogowania, którego kliknięcie przełączy na widok formularza logowania
- zamiast `witaj przybyszu` dajemy email użytkownika np.: `Witaj test@op.pl`

 #### Dodatkowo proponuje zapisywać informacje o zalogowanym użytkowniku w cookies